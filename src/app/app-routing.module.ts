import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ComponentsModule } from './components/components.module';
import { RegisterComponent } from './pages/register/register.component';
import { FoodServicesComponent } from './pages/food-services/food-services.component';
import { CourierServicesComponent } from './pages/courier-services/courier-services.component';
import { HomeCourierComponent } from './pages/courier-services/home/home.component';
import { PaymentMethodCourierComponent } from './pages/courier-services/payment-method/payment-method.component';
import { HistoryComponent } from './pages/courier-services/history/history.component';
import { FoodDeliveryComponent } from './pages/food-delivery/food-delivery.component';
import { HomeFoodDeliveryComponent } from './pages/food-delivery/home/home.component';
import { PaymentMethodFoodDeliveryComponent } from './pages/food-delivery/payment-method/payment-method.component';
import { MyOrdersComponent } from './pages/food-delivery/my-orders/my-orders.component';
import { DeliveryDetailsComponent } from './pages/courier-services/delivery-details/delivery-details.component';
import { CheckoutComponent } from './pages/food-delivery/checkout/checkout.component';
import { FilterComponent } from './pages/food-delivery/filter/filter.component';
import { NearbyDispatcherComponent } from './pages/courier-services/nearby-dispatcher/nearby-dispatcher.component';
import { RestaurantComponent } from './pages/food-delivery/restaurant/restaurant.component';
import { DispatcherComponent } from './pages/courier-services/dispatcher/dispatcher.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'food-services', component: FoodServicesComponent },
    { path: 'courier', component: CourierServicesComponent, children: [
        { path: '', pathMatch: 'full', redirectTo: 'home' },
        { path: 'home', component: HomeCourierComponent },
        { path: 'payment-method', component: PaymentMethodCourierComponent },
        { path: 'history', component: HistoryComponent },
        { path: 'delivery-details', component: DeliveryDetailsComponent },
        { path: 'nearby-dispatchers', component: NearbyDispatcherComponent },
        { path: 'dispatcher', component: DispatcherComponent }
    ]},
    { path: 'food-delivery', component: FoodDeliveryComponent, children: [
        { redirectTo: 'home', path: '', pathMatch: 'full' },
        { path: 'home', component: HomeFoodDeliveryComponent },
        { path: 'payment-method', component: PaymentMethodFoodDeliveryComponent },
        { path: 'my-orders', component: MyOrdersComponent },
        { path: 'checkout', component: CheckoutComponent },
        { path: 'filter', component: FilterComponent },
        { path: 'restaurant', component: RestaurantComponent }
    ]},
];

@NgModule({
    imports: [
        ComponentsModule,
        RouterModule.forRoot(routes)
    ],
    exports: [ RouterModule ],
    declarations: [
        HomeComponent,
        RegisterComponent,
        FoodServicesComponent,
        CourierServicesComponent,
        HomeCourierComponent,
        PaymentMethodCourierComponent,
        HistoryComponent,
        FoodDeliveryComponent,
        HomeFoodDeliveryComponent,
        PaymentMethodFoodDeliveryComponent,
        MyOrdersComponent,
        DeliveryDetailsComponent,
        CheckoutComponent,
        FilterComponent,
        NearbyDispatcherComponent,
        RestaurantComponent,
        DispatcherComponent
    ]
})
export class AppRoutingModule {}
