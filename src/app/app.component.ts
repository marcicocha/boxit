import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'boxit';

  constructor() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDl-lkILfqyHYj6sFZFFO6Qds43LDNxNoo',
      authDomain: 'boxitwebapp.firebaseapp.com',
      databaseURL: 'https://boxitwebapp.firebaseio.com',
      projectId: 'boxitwebapp',
      storageBucket: 'boxitwebapp.appspot.com',
      messagingSenderId: '416431812470'
    });
  }
}
