import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { TabDirectiveDirective } from './tab-directive.directive';
import { ComponentsModule } from './components/components.module';



@NgModule({
  declarations: [
    AppComponent,
    TabDirectiveDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
