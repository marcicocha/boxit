import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { HomeNavComponent } from './home-nav/home-nav.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { DispatchBannerComponent } from './dispatch-banner/dispatch-banner.component';
import { TestimonyComponent } from './testimony/testimony.component';
import { CourierTabComponent } from './courier-tab/courier-tab.component';
import { FoodDeliveryTabComponent } from './food-delivery-tab/food-delivery-tab.component';
import { GooglePlayComponent } from './google-play/google-play.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { RestaurantCardComponent } from './restaurant-card/restaurant-card.component';
import { SignInModalComponent } from './sign-in-modal/sign-in-modal.component';


@NgModule({
    imports: [ RouterModule, CommonModule ],
    exports: [
        FooterComponent,
        HomeNavComponent,
        SponsorsComponent,
        DispatchBannerComponent,
        TestimonyComponent,
        CourierTabComponent,
        FoodDeliveryTabComponent,
        GooglePlayComponent,
        NewsletterComponent,
        RestaurantCardComponent
    ],
    declarations: [
        FooterComponent,
        HomeNavComponent,
        SponsorsComponent,
        DispatchBannerComponent,
        TestimonyComponent,
        CourierTabComponent,
        FoodDeliveryTabComponent,
        GooglePlayComponent,
        NewsletterComponent,
        RestaurantCardComponent,
        SignInModalComponent
    ],
    bootstrap: []
})
export class ComponentsModule {}
