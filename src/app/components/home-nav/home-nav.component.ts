import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $;

@Component({
  selector: 'app-home-nav',
  templateUrl: './home-nav.component.html',
  styleUrls: ['./home-nav.component.scss']
})
export class HomeNavComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    $(window).scroll(function() {
      const scroll = $(window).scrollTop();
      if (scroll >= 40) {
          $('.navbar').addClass('fixed-top');
      } else {
          $('.navbar').removeClass('fixed-top');
      }
    });
  }

  navigatePage() {
    this.router.navigate(['home']);
  }

}
