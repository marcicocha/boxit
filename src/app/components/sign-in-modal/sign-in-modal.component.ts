import { Component, OnInit, ElementRef, Renderer2, ViewChild  } from '@angular/core';

@Component({
  selector: 'app-sign-in-modal',
  templateUrl: './sign-in-modal.component.html',
  styleUrls: ['./sign-in-modal.component.css']
})
export class SignInModalComponent implements OnInit {
  @ViewChild('tabone') tabone: ElementRef;
  @ViewChild('tabtwo') tabtwo: ElementRef;
  @ViewChild('signin') signin: ElementRef;
  @ViewChild('signup') signup: ElementRef;

  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.renderer.addClass(this.tabone.nativeElement, 'show');
    this.renderer.addClass(this.tabone.nativeElement, 'active');
    this.renderer.addClass(this.signin.nativeElement, 'active');
  }

  signIn() {
    this.renderer.addClass(this.tabone.nativeElement, 'show');
    this.renderer.addClass(this.tabone.nativeElement, 'active');

    this.renderer.removeClass(this.tabtwo.nativeElement, 'show');
    this.renderer.removeClass(this.tabtwo.nativeElement, 'active');

    this.renderer.addClass(this.signin.nativeElement, 'active');
    this.renderer.removeClass(this.signup.nativeElement, 'active');
  }

  signUp() {
    this.renderer.addClass(this.tabtwo.nativeElement, 'show');
    this.renderer.addClass(this.tabtwo.nativeElement, 'active');

    this.renderer.removeClass(this.tabone.nativeElement, 'show');
    this.renderer.removeClass(this.tabone.nativeElement, 'active');

    this.renderer.removeClass(this.signin.nativeElement, 'active');
    this.renderer.addClass(this.signup.nativeElement, 'active');
  }

}
